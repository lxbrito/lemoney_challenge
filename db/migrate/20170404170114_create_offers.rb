class CreateOffers < ActiveRecord::Migration[5.0]
  def change
    create_table :offers do |t|
      t.string :advertiser_name
      t.string :url
      t.text :description
      t.datetime :starts_at
      t.datetime :ends_at
      t.integer :offer_type, null: false, default: 0
      t.integer :status, null: false, default: 0

      t.timestamps
    end
    add_index :offers, :advertiser_name, unique: true
  end
end

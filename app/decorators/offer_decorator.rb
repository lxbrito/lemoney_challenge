class OfferDecorator < Draper::Decorator
  delegate_all

  def togle_status_link
    if object.enabled?
      disable_link
    else
      enable_link
    end
  end

  private
  def disable_link
    h.link_to 'Disable', h.disable_admin_offer_path(object)
  end

  def enable_link
    h.link_to 'Enable', h.enable_admin_offer_path(object)
  end
end
module Admin
  class ApplicationController < ActionController::Base
    layout 'admin'
    include Pundit
    protect_from_forgery with: :exception
    before_action :authenticate_user!
    before_action :authorize_admin!

    private
    def authorize_admin!
      #very basic don't need yet to use Pundit
      if !current_user.admin?
        return redirect_to '/', notice: "You don't have permissions"
      end
    end
  end
end
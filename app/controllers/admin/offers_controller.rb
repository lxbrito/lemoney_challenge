module Admin
  class OffersController < Admin::ApplicationController
    before_action :set_offer, only: [:show, :edit, :update, :destroy, :enable, :disable]
    respond_to :html
    # GET /offers
    def index
      @offers = ::OfferDecorator.decorate_collection(Offer.all)
    end

    # GET /offers/1
    def show
    end

    # GET /offers/new
    def new
      @offer = Offer.new
    end

    # GET /offers/1/edit
    def edit
    end

    # POST /offers
    def create
      @offer = Offer.new(offer_params)
      if @offer.save
        redirect_to admin_offers_url, notice: 'Offer was successfully created.'
      else
        render :new
      end
    end

    # PATCH/PUT /offers/1
    # PATCH/PUT /offers/1.json
    def update
      if @offer.update(offer_params)
        redirect_to admin_offers_url, notice: 'Offer was successfully updated.'
      else
        render :edit
      end
    end

    #Don't like toggle because of consistency problems
    def enable
      if @offer.enabled!
        redirect_to admin_offers_url, notice: 'Offer was successfully enabled.'
      else
        redirect_to admin_offers_url, error: "Couldn't handle the request." #just in case - unreachable code
      end
    end

    def disable
      if @offer.disabled!
        redirect_to admin_offers_url, notice: 'Offer was successfully disabled.'
      else
        redirect_to admin_offers_url, error: "Couldn't handle the request." #just in case - unreachable code
      end

    end

    # DELETE /offers/1
    # DELETE /offers/1.json
    def destroy
      @offer.destroy
      redirect_to admin_offers_url, notice: 'Offer was successfully destroyed.'
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_offer
        @offer = Offer.find(params[:id])
      end

      # Never trust parameters from the scary internet, only allow the white list through.
      def offer_params
        params.require(:offer).permit(:advertiser_name, :url, :description, :starts_at, :ends_at, :offer_type)
      end
  end
end

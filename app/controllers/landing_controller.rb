class LandingController < ApplicationController

  def index
    @offers = Offer.active.order(offer_type: 'desc')
  end
end

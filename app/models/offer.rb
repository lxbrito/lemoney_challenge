# == Schema Information
#
# Table name: offers
#
#  id              :integer          not null, primary key
#  advertiser_name :string
#  url             :string
#  description     :text
#  starts_at       :datetime
#  ends_at         :datetime
#  offer_type      :integer          default("regular"), not null
#  status          :integer          default("disabled"), not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_offers_on_advertiser_name  (advertiser_name) UNIQUE
#

class Offer < ApplicationRecord
  enum offer_type:[:regular, :premium]
  enum status:[:disabled, :enabled]

  #TODO move validations to Reform form object
  validates_presence_of :advertiser_name
  validates_uniqueness_of :advertiser_name
  validates :description, presence: true, length: { maximum: 500 }
  validates :url, presence: true, :format => URI::regexp(%w(http https))
  validates_presence_of :starts_at

  scope :active, -> { where(status:Offer.statuses[:enabled]).where("starts_at <= ?", Time.now).where("ends_at >= ? OR ends_at IS NULL", Time.now)}
end

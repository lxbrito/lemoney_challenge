module ControllerMacros
  def login_user
    before do
      sign_in FactoryGirl.create(:user)
    end
  end

  def login_admin
    before do
      sign_in FactoryGirl.create(:admin_user)
    end
  end
end


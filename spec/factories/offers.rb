FactoryGirl.define do
  factory :offer do
    advertiser_name {Faker::Company.name}
    url {Faker::Internet.url}
    description {Faker::Company.buzzword}
    starts_at { 2.days.ago}
    ends_at { 1.day.from_now }
    offer_type {Offer.offer_types[:regular]}
    status {Offer.statuses[:enabled] }
  end

  factory :premium_offer, parent: 'offer' do
    offer_type {Offer.offer_types[:premium]}
  end

  factory :premium_offer_expired, parent: 'premium_offer' do
    ends_at { 1.minute.ago }
  end

  factory :premium_offer_not_started, parent: 'premium_offer' do
    starts_at { 2.minutes.from_now }
  end

  factory :premium_offer_disabled, parent: 'premium_offer' do
    status {Offer.statuses[:disabled] }

  end

  factory :regular_offer_expired, parent: 'offer' do
    ends_at { 1.minute.ago }
  end

  factory :regular_offer_not_started, parent: 'offer' do
    starts_at { 2.minutes.from_now }
  end

  factory :regular_offer_disabled, parent: 'offer' do
    status {Offer.statuses[:disabled] }
  end

  factory :invalid_offer, parent: 'offer' do
    url "not a valid url"
  end

end

FactoryGirl.define do
  factory :user do
    email { Faker::Internet.free_email(Faker::Name.name ) }
    password "12341234"
    role { User.roles[:customer]}
  end

  factory :admin_user, parent: 'user' do
    role { User.roles[:admin]}
  end
end

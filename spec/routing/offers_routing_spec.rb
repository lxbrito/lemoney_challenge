require "rails_helper"

RSpec.describe Admin::OffersController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/admin/offers").to route_to("admin/offers#index")
    end

    it "routes to #new" do
      expect(:get => "/admin/offers/new").to route_to("admin/offers#new")
    end

    it "routes to #show" do
      expect(:get => "/admin/offers/1").to route_to("admin/offers#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/admin/offers/1/edit").to route_to("admin/offers#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/admin/offers").to route_to("admin/offers#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/admin/offers/1").to route_to("admin/offers#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/admin/offers/1").to route_to("admin/offers#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/admin/offers/1").to route_to("admin/offers#destroy", :id => "1")
    end

    it "routes to #enable" do
      expect(:get => "/admin/offers/1/enable").to route_to("admin/offers#enable", :id => "1")
    end

    it "routes to #disable" do
      expect(:get => "/admin/offers/1/disable").to route_to("admin/offers#disable", :id => "1")
    end

  end
end

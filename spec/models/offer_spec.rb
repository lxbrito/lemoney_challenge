require 'rails_helper'

RSpec.describe Offer, type: :model do
  # validates_presence_of :advertiser_name
  # validates_uniqueness_of :advertiser_name
  # validates :description, presence: true, length: { maximum: 500 }
  # validates :url, presence: true, :format => URI::regexp(%w(http https))
  # validates_presence_of :starts_at

  it "is valid with valid attributes" do

  end

  it { is_expected.to validate_uniqueness_of(:advertiser_name) }
  it { is_expected.to validate_presence_of(:advertiser_name) }

  it "is not valid without a description" do

  end

  it "is not valid with a description longer than 500" do

  end

  it "is not valid without an url" do

  end

  it "is not valid with a malformed url" do

  end

  it "is not valid without start_at" do

  end
end

require 'rails_helper'

RSpec.describe LandingController, type: :controller do
  login_user
  let(:valid_session) { {} }

  describe "GET #index" do
    it "assigns all active offers as @offers" do
      active_offers = [FactoryGirl.create(:offer), FactoryGirl.create(:premium_offer)]
      inactive_offers = [FactoryGirl.create(:regular_offer_expired), FactoryGirl.create(:regular_offer_not_started), FactoryGirl.create(:regular_offer_disabled), FactoryGirl.create(:premium_offer_expired), FactoryGirl.create(:premium_offer_not_started), FactoryGirl.create(:premium_offer_disabled)]
      get :index, params: {}, session: valid_session
      expect(assigns(:offers).to_a).to match_array(active_offers)
    end
  end
end

require 'rails_helper'

RSpec.describe "admin/offers/new", type: :view do
  before(:each) do
    assign(:offer, FactoryGirl.build(:offer))
  end

  it "renders new offer form" do
    render

    assert_select "form[action=?][method=?]", admin_offers_path, "post" do

      assert_select "input#offer_advertiser_name[name=?]", "offer[advertiser_name]"

      assert_select "input#offer_url[name=?]", "offer[url]"

      assert_select "textarea#offer_description[name=?]", "offer[description]"

      assert_select "input#offer_offer_type[name=?]", "offer[offer_type]"
    end
  end
end

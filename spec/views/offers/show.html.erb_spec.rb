require 'rails_helper'

RSpec.describe "admin/offers/show", type: :view do
  before(:each) do
    @offer = assign(:offer, FactoryGirl.create(:offer))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(@offer.advertiser_name)
    expect(rendered).to match(@offer.url)
    expect(rendered).to match(@offer.description)
    expect(rendered).to match(/regular/)
  end
end

require 'rails_helper'

RSpec.describe "admin/offers/index", type: :view do
  before(:each) do
    @offers = assign(:offers, [
        FactoryGirl.create(:premium_offer).decorate,
        FactoryGirl.create(:offer).decorate
    ])
  end

  it "renders a list of offers" do
    render
    assert_select "tr>td", :text => @offers[0].advertiser_name, :count => 1
    assert_select "tr>td", :text => @offers[1].advertiser_name, :count => 1
    assert_select "tr>td", :text => @offers[0].url, :count => 1
    assert_select "tr>td", :text => @offers[1].url, :count => 1
    assert_select "tr>td", :text => @offers[1].description, :count => 1
    assert_select "tr>td", :text => @offers[1].description, :count => 1
    assert_select "tr>td", :text => "premium", :count => 1
    assert_select "tr>td", :text => "regular", :count => 1
  end
end

# == Route Map
#
#                   Prefix Verb   URI Pattern                      Controller#Action
#                     root GET    /                                landing#index
#         new_user_session GET    /users/sign_in(.:format)         devise/sessions#new
#             user_session POST   /users/sign_in(.:format)         devise/sessions#create
#     destroy_user_session DELETE /users/sign_out(.:format)        devise/sessions#destroy
# cancel_user_registration GET    /users/cancel(.:format)          devise/registrations#cancel
#    new_user_registration GET    /users/sign_up(.:format)         devise/registrations#new
#   edit_user_registration GET    /users/edit(.:format)            devise/registrations#edit
#        user_registration PATCH  /users(.:format)                 devise/registrations#update
#                          PUT    /users(.:format)                 devise/registrations#update
#                          DELETE /users(.:format)                 devise/registrations#destroy
#                          POST   /users(.:format)                 devise/registrations#create
#             admin_offers GET    /admin/offers(.:format)          admin/offers#index
#                          POST   /admin/offers(.:format)          admin/offers#create
#          new_admin_offer GET    /admin/offers/new(.:format)      admin/offers#new
#         edit_admin_offer GET    /admin/offers/:id/edit(.:format) admin/offers#edit
#              admin_offer GET    /admin/offers/:id(.:format)      admin/offers#show
#                          PATCH  /admin/offers/:id(.:format)      admin/offers#update
#                          PUT    /admin/offers/:id(.:format)      admin/offers#update
#                          DELETE /admin/offers/:id(.:format)      admin/offers#destroy
#

Rails.application.routes.draw do
  root 'landing#index'

  devise_for :users

  namespace :admin do
    root 'offers#index'
    resources :offers do
      get 'enable', on: :member
      get 'disable', on: :member
    end
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
